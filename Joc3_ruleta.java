/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joc3_ruleta;
import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author tarda
 */
public class Joc3_ruleta {
/**
     * Funció AfegirPunts: Demana per teclat quants punts volem afegir al Saldo
     *
     * @saldo : Rep el saldo total del que disposa
     *
     * @return : Retorna el Saldo + el nº de punts que ha ingressat
     *
     */
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int saldo = 100;
        int aposta = 50;
        int tipus;
        System.out.printf("El teu saldo actual és: %d\n", saldo);
        while (apostaPunts(saldo, aposta) != -1) {
            tipus = tipusAposta();
            saldo = joc(tipus, saldo, aposta);
            System.out.printf("El teu saldo actual és: %d\n", saldo);
            System.out.println("cuanto quiere apostar");
            aposta=sc.nextInt();
            
            
        }
    }

    /**
     * Funció pintaAsterisc: Fes una funció que rebi per paràmetre un número
     * sencer i pinti per pantalla tants asteriscs com el número.
     *
     * @numero: sencer que indica el nº d'asteriscs
     *
     */
    static void pintaAsterisc(int numero) {
        for(int i=0;i<numero;i++){
            System.out.print("*");
        }
    }

    /**
     * Funció aleatori: Dissenya una funció que ens retorni un número aleatori
     * entre el 0 i el 36 inclosos
     *
     * @return : nº sencer entre 0 i 36
     */
    static int aleatori() {
        Random r = new Random(); 
        int val = r.nextInt(36)+1;
        
       
        return val;
    }

    /**
     * Funció: tipusAposta Dissenya una funció que demani a l'usuari quin tipus
     * d'aposta vol fer
     *
     * @1return : Nº sencer -1 : aposta és SENAR -2: aposta és PARELL 1 i 36 quan
     * aposta per un número
     *
     */
    static int tipusAposta() {
        int n;
        Scanner sc=new Scanner(System.in);
        
        do{
        System.out.println("-1 : aposta és SENAR \n -2: aposta és PARELL\n 1 i 36 quan aposta per un número");   
            n=sc.nextInt();
        }while(n<-2||n>36);
        return n;  
    }

    /**
     * Funcio: apostaPunts Dissenya una funció que demani a l'usuari quants
     * punts vol apostar i els retorni.No pot apostar més punts dels que
     * disposa.
     *
     * @puntsTotal : Nº sencer de punts total de l'usuari
     * @puntsAposta : Nº sencer de punts que aposta
     *
     * @return : nº sencer -1 quan puntsTotal es menor a la aposta retorna els
     * puntsAposta
     */
    static int apostaPunts(int puntsTotal, int puntsAposta) {
        if( puntsTotal<puntsAposta){
        return -1; 
        }else{
            return puntsAposta;
        }
    }

    /**
     * Dissenya una funció que faci joc amb l'aposta: La funció ha de rebre per
     * paràmetre:
     *
     * @tipusAposta : sencer amb quin tipus d'aposta farà.
     * @saldo : sencer amb el saldo actual de punts.
     * @puntsAposta : sencer quantitat de punts aposta.
     *
     *
     * @return : sencer torna el saldo de punts que ens queda després de fer
     * l'aposta
     *
     */
    static int joc(int tipusAposta, int saldo, int puntsAposta) {
        int b;
        b=aleatori();
        switch(tipusAposta){
            case -1:
                if(b%2==1)return saldo+tipusAposta;
                else return saldo-tipusAposta;
            case -2:
                if(b%2==0)return saldo+tipusAposta;
                else return saldo-tipusAposta;
            case 0:
                if(b%2==1)return saldo+tipusAposta;
                else return saldo-tipusAposta;
            default:
                if(b==tipusAposta)return saldo+(tipusAposta*2);
                else return saldo-tipusAposta;
                
        
        }
    }

    /**
     * Funció QuantitatAposta: Demana per teclat quina quantitat vol apostar
     *
     * @return : nº sencer amb la quantitat que vol apostar 0 si no vol apostar
     * més
     *
     */
    /**
     * Funció AfegirPunts: Demana per teclat quants punts volem afegir al Saldo
     *
     * @saldo : Rep el saldo total del que disposa
     *
     * @return : Retorna el Saldo + el nº de punts que ha ingressat
     *
     */

}
